# encoding: utf-8

from command import createsuperuser
from command.captcha import captcha


commands = (createsuperuser.createsuperuser, captcha.captcha)
