# encoding: utf-8

import shutil
import sys
import time

import click
from flask import current_app
from flask.cli import with_appcontext

from command.captcha import audio, solution, visual


ERROR_MAX = 5


@click.group()
@click.pass_context
def captcha(context):
    "Tools for Captcha."
    context.ensure_object(dict)


@captcha.command(help="Generates new challenges.")
@click.option(
    "--local",
    is_flag=True,
    default=False,
    help="Local mode - generate new challenges from existing sources.",
)
@click.pass_context
@with_appcontext
def generate(context, local):
    start_time = time.perf_counter()
    visual_challenges = 0
    error_count = 0
    click.echo("Starting generation of visual challenges")
    nb_challenges = visual.get_number_of_active_challenges()
    click.echo(
        f"There are currently {nb_challenges} active visual challenges"
        f" on a minimum of {current_app.config['VISUAL_CHALLENGE_MIN']}."
    )
    if nb_challenges < current_app.config["VISUAL_CHALLENGE_MIN"]:
        click.echo(
            f"Generating up to {current_app.config['VISUAL_CHALLENGE_MAX']} "
            "visual challenges."
        )
        if local:
            sources = visual.get_sources()
            if len(sources) < 1:
                click.echo("No local source!")
                return
        index = 0
        while nb_challenges < current_app.config["VISUAL_CHALLENGE_MAX"]:
            picture = None
            if local:
                source = {
                    "source": sources[index],
                    "original": visual.get_source_image(sources[index].filepath),
                }
                index += 1
            else:
                picture = visual.get_picture()
                if picture is None:
                    error_count += 1
                    if error_count > ERROR_MAX:
                        click.echo("")
                        click.echo("Too much errors on visual generation!")
                        return
                    continue
                source = visual.create_source(picture)
            try:
                if source is not None:
                    # Create new challenges from this picture
                    for idx in range(current_app.config["VISUAL_CHALLENGE_BY_SOURCE"]):
                        if visual.create_challenge(
                            source["source"].id, source["original"]
                        ):
                            nb_challenges += 1
                            visual_challenges += 1
                            click.echo(".", nl=False)
                            if error_count > 0:
                                error_count -= 1
                else:
                    click.echo(".", nl=False)
            except Exception as e:
                if picture is not None:
                    click.echo(picture["source_url"])
                    click.echo(picture["file_url"])
                click.echo("    => %s" % str(e))
                error_count += 1
                if error_count > ERROR_MAX:
                    click.echo("")
                    click.echo("Too much errors on visual generation!")
                    return
                continue
            click.echo(".", nl=False)
            sys.stdout.flush()
        click.echo("")
        click.echo(
            f"{visual_challenges} visual challenges generated "
            f"in {time.perf_counter() - start_time} seconds"
        )
        click.echo("")
        click.echo("Finished!")
    else:
        click.echo("No need to generate visual challenges.")

    start_time = time.perf_counter()
    audio_challenges = 0
    error_count = 0
    click.echo("")
    click.echo("Starting generation of audio challenges")
    nb_challenges = audio.get_number_of_active_challenges()
    click.echo(
        f"There are currently {nb_challenges} active audio challenges"
        f" on a minimum of {current_app.config['AUDIO_CHALLENGE_MIN']}."
    )
    if nb_challenges < current_app.config["AUDIO_CHALLENGE_MIN"]:
        click.echo(
            f"Generating up to {current_app.config['AUDIO_CHALLENGE_MAX']} "
            "audio challenges."
        )
        index = 0
        while nb_challenges < current_app.config["AUDIO_CHALLENGE_MAX"]:
            sound = None
            music = audio.get_music()
            if music is None:
                error_count += 1
                if error_count > ERROR_MAX:
                    click.echo("")
                    click.echo("Too much errors on audio generation!")
                    return
                continue
            try:
                if music is not None:
                    # Create new challenges from this music
                    for idx in range(current_app.config["AUDIO_CHALLENGE_BY_SOURCE"]):
                        if audio.create_challenge(music):
                            nb_challenges += 1
                            audio_challenges += 1
                            click.echo(".", nl=False)
                            if error_count > 0:
                                error_count -= 1
                    # Clean music file
                    os.remove(music["file_path"])
                else:
                    click.echo(".", nl=False)
            except Exception as e:
                click.echo(music["song_url"])
                click.echo("    => %s" % str(e))
                error_count += 1
                if error_count > ERROR_MAX:
                    click.echo("")
                    click.echo("Too much errors on audio generation!")
                    audio.clean_files()
                    return
                continue
            click.echo(".", nl=False)
            sys.stdout.flush()
        audio.clean_files()
        click.echo("")
        click.echo(
            f"{audio_challenges} audio challenges generated "
            f"in {time.perf_counter() - start_time} seconds"
        )
        click.echo("")
        click.echo("Finished!")
    else:
        click.echo("No need to generate audio challenges.")


@captcha.command(help="Clean disabled challenges and outdated solutions.")
@click.option(
    "--all", is_flag=True, default=False, help="Clean all challenges and solutions."
)
@click.pass_context
@with_appcontext
def clean(context, all):
    if all:
        click.echo("Cleaning all challenges")
        # Cleaning files
        click.echo(".", nl=False)
        shutil.rmtree(current_app.config["VISUAL_CHALLENGE_DIR"], ignore_errors=True)
        click.echo(".", nl=False)
        shutil.rmtree(current_app.config["AUDIO_CHALLENGE_DIR"], ignore_errors=True)
        # Cleaning data
        click.echo(".", nl=False)
        visual.delete_all_challenges()
        click.echo(".", nl=False)
        audio.delete_all_challenges()
    else:
        click.echo("Cleaning inactive challenges")
        # Visual challenges
        challenges = visual.get_inactive_challenges()
        click.echo(f"There are currently {len(challenges)} inactive visual challenges.")
        for challenge in challenges:
            visual.delete_challenge(challenge)
            click.echo(".", nl=False)

        # Audio challenges
        challenges = audio.get_inactive_challenges()
        click.echo(f"There are currently {len(challenges)} inactive audio challenges.")
        for challenge in challenges:
            audio.delete_challenge(challenge)
            click.echo(".", nl=False)

    click.echo("")
    click.echo("Finished!")


@captcha.command(help="Reset used challenges.")
@click.pass_context
@with_appcontext
def reset(context):
    click.echo("Reseting visual challenges")
    visual.reset_challenges()
    click.echo("Reseting audio challenges")
    audio.reset_challenges()
    click.echo("Finished!")


@captcha.command(help="Display some stats about this instance.")
@click.pass_context
@with_appcontext
def stats(context):
    click.echo("Visual challenges")
    click.echo("-"* 42)
    challenges = len(visual.get_all_challenges())
    actives = visual.get_number_of_active_challenges()
    inactives = challenges - actives
    click.echo(f"{challenges} challenges")
    click.echo(f"- {actives} actives")
    click.echo(f"- {inactives} inactives")
    click.echo("="* 42)

    click.echo("Audio challenges")
    click.echo("-"* 42)
    challenges = len(audio.get_all_challenges())
    actives = audio.get_number_of_active_challenges()
    inactives = challenges - actives
    click.echo(f"{challenges} challenges")
    click.echo(f"- {actives} actives")
    click.echo(f"- {inactives} inactives")
    click.echo("="* 42)
