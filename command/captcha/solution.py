# encoding: utf-8
"""
This module is used in order to manage solutions
"""

from datetime import datetime, timedelta

from flask import current_app
from app.model.solution import SolutionModel


def get_all_solutions():
    return SolutionModel.query.all()


def get_outdated_solutions():
    # We take some more time in order not to cross complicate solution
    duration = current_app.config["SOLUTION_DURATION"] + 2
    return SolutionModel.query.filter(
        SolutionModel.date<=(datetime.now() - timedelta(duration))
    ).all()
