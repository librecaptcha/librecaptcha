# encoding: utf-8
"""
This module is used in order to create audio challenges.
"""

import glob
from urllib.parse import unquote
import os
import random
import time
import xml.etree.ElementTree

from bs4 import BeautifulSoup
import click
from flask import current_app
import requests

from app.model.audio_challenge import AudioChallengeModel
from command.captcha import utils


MUSICS_URL = "https://play.dogmazic.net/server/ajax.server.php?page=random&action=song"
PART_DURATION = 15
FFMPEG_BASE = "ffmpeg -hide_banner -loglevel panic -y "
SLEEP_TIME = 0.3


class Musics:
    musics = []


def get_number_of_active_challenges():
    return AudioChallengeModel.query.filter(
        AudioChallengeModel.used<current_app.config["MAX_USED"]
    ).count()


def get_inactive_challenges():
    return AudioChallengeModel.query.filter(
        AudioChallengeModel.used>=current_app.config["MAX_USED"]
    ).all()


def get_all_challenges():
    return AudioChallengeModel.query.all()


def get_music():
    if len(Musics.musics) == 0:
        # Renew musics cache
        time.sleep(SLEEP_TIME)
        try:
            click.echo("C", nl=False)
            result = requests.get(MUSICS_URL, headers=utils.USER_AGENT)
            root = xml.etree.ElementTree.fromstring(result.text)
            soup = BeautifulSoup(root.find("content").text, "html.parser")
            ul = soup.find("ul", {"id": "rb_current_playlist"})
            Musics.musics = [li.find("a")["href"] for li in ul.find_all("li")]
        except Exception as e:
            click.echo("")
            click.echo("Error while caching new musics list")
            click.echo("    => %s" % str(e))
            return None

    try:
        time.sleep(SLEEP_TIME)
        song_url = Musics.musics.pop()
        song_id = song_url.split("=")[-1]
        result = requests.get(song_url)
        soup = BeautifulSoup(result.text, "html.parser")
        duration = soup.find("dt", text="Durée").find_next_sibling("dd").text
        if utils.duration_in_seconds(duration) < 3 * PART_DURATION:
            click.echo("S", nl=False)
            return None
        license = soup.find("dt", text="Licences").find_next_sibling("dd").text
        if not utils.accepted_license(license):
            click.echo("L", nl=False)
            return None
        author = soup.find("dt", text="Artiste").find_next_sibling("dd").text
        title = unquote(soup.find("img", alt="Lien").parent["href"].split("=")[-1])
        extension = title.split(".")[-1]
        song_file = f"{song_id}.{extension}"
    except Exception as e:
        click.echo("    => %s" % str(e))
        return None

    audio_id = "%064x" % random.getrandbits(256)
    os.makedirs(current_app.config["AUDIO_SOURCE_DIR"])
    music = {
        "file_path": f"{current_app.config['AUDIO_SOURCE_DIR']}/{song_file}",
        "file_name": song_file,
        "file_url": song_url,
        "file_ext": extension,
        "license": license,
        "author": author,
        "duration": utils.duration_in_seconds(duration),
    }
    click.echo(".", nl=False)
    result = requests.get(
        f"https://play.dogmazic.net/stream.php?action=download&song_id={song_id}",
        stream=True,
        headers=utils.USER_AGENT
    )
    with open(f"{music['file_path']}", "wb") as f:
        for chunk in result.iter_content(chunk_size=1048576):
            f.write(chunk)
    return music


def create_challenge(music):
    # Each challenge is a part of a music with some beeps in it
    try:
        token = "%064x" % random.getrandbits(256)
        challenge_path = os.path.join(
            current_app.config["AUDIO_CHALLENGE_DIR"],
            token[:2],
            token[2:4],
            token[4:6],
        )
        os.makedirs(challenge_path)

        # Get part of orignal music
        offset = random.randint(0, music["duration"] - PART_DURATION)
        part_file = "/tmp/librecaptcha_part.ogg"
        os.system(
            f"{FFMPEG_BASE} -i {music['file_path']} -vn -acodec libvorbis "
            f"-ss {utils.seconds_in_duration(offset)} -t {PART_DURATION} {part_file}"
        )

        # Generating beeps
        beep_file = "/tmp/librecaptcha_beep.ogg"
        beep_counter = 0
        previous_beep_type = 0
        duration = 0
        # Total time of beep sound
        total_time = 0
        # Beep type : short or long
        beep_type = None
        # Solution of challenge
        solution = ""
        while total_time < ((PART_DURATION - 2) * 1000):
            previous_beep_type = beep_type
            beep_type = random.randint(0, 1)
            frequency = random.randint(80, 160) * 10
            if beep_type == 0:
                solution += "."
                duration = 0.2
            else:
                solution += "-"
                duration = 0.6
            os.system(
                f"{FFMPEG_BASE} -f lavfi "
                f'-i "sine=frequency={frequency}:duration={duration}" '
                f'-af "volume=20dB" {beep_file}'
            )
            if previous_beep_type is None:
                total_time += random.randint(12, 24) * 100
            elif previous_beep_type == 0:
                total_time += random.randint(8, 16) * 100
            else:
                total_time += random.randint(12, 20) * 100
            os.system(
                    f'{FFMPEG_BASE} -i {beep_file} -af "adelay={total_time}" '
                    f"/tmp/librecaptcha_beep_{beep_counter}.ogg"
                )
            beep_counter += 1

        # Merging results
        command = f"{FFMPEG_BASE} -i {part_file}"
        for idx in range(beep_counter):
            command += f" -i /tmp/librecaptcha_beep_{idx}.ogg"
        command += (
            f" -filter_complex amix=inputs={beep_counter+1}:duration=longest "
            f"{challenge_path}/{token}.ogg"
        )
        os.system(command)

        # Save challenge
        AudioChallengeModel(
            token=token,
            used=0,
            solution=solution,
            author=music["author"],
            license=music["license"],
        ).save()
        return True
    except Exception as e:
        click.echo("    => %s" % str(e))
        if "group" in str(e):
            click.echo("    => %s" % result)
        return False


def clean_files():
    # Clean temporary files
    for filename in glob.glob("/tmp/librecaptcha*.ogg"):
        os.remove(filename)


def delete_challenge(challenge):
    challenge_filepath = challenge.filepath
    challenge.delete()
    try:
        os.remove(challenge_filepath)
    except Exception as e:
        click.echo(f"    => {str(e)}")
    utils.clean_folders(os.path.dirname(challenge_filepath))


def delete_all_challenges():
    AudioChallengeModel.truncate()


def reset_challenges():
    AudioChallengeModel.execute(
        f"update {AudioChallengeModel.__tablename__} set used=0;"
    )
