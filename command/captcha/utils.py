# encoding: utf-8
"""
This module is used to provide some utilities to captcha commands.
"""

import os


ACCEPTED_LICENSES = (
    "GFDL",
    "CC0",
    "CC BY",
)
USER_AGENT = {
    "User-agent": "Mozilla/5.0; LibreCaptchaBot/0.4; +https://librecaptcha.org/bot)"
}


def normalize_license(license):
    """
    For now, licenses could be CC0, CC XX, GFDL.
    If license is not known, it will be reused directly.
    """
    result = license
    if "zero" in license or "cc0" in license:
        result = "CC0"
    elif "by" in license:
        result = "CC BY"
        if "nc" in license:
            result += "-NC"
        if "nd" in license:
            result += "-ND"
        if "sa" in license:
            result += "-SA"
        if "1.0" in license:
            result += " 1.0"
        elif "2.0" in license:
            result += " 2.0"
        elif "2.1" in license:
            result += " 2.1"
        elif "2.5" in license:
            result += " 2.5"
        elif "3.0" in license:
            result += " 3.0"
        elif "4.0" in license:
            result += " 4.0"
    elif "gfdl" in license:
        result = "GFDL"
        if "1.1" in license:
            result += " 1.1"
        elif "1.2" in license:
            result += " 1.2"
        elif "1.3" in license:
            result += " 1.3"
    return result


def accepted_license(license):
    # No Derivative refused
    if "nd" in license.lower():
        return False
    # No Commercial refused
    if "nc" in license.lower() or "noncommercial" in license.lower():
        return False
    # only Creative Commons licenses accepted for now
    if license.lower().startswith("Creative Commons - by"):
        return True
    return False


def clean_folders(folder):
    if os.listdir(folder) == []:
        try:
            os.rmdir(folder)
        except Exception as e:
            click.echo(f"    => {str(e)}")
    folder = folder[:-3]
    if os.listdir(folder) == []:
        try:
            os.rmdir(folder)
        except Exception as e:
            click.echo(f"    => {str(e)}")
    folder = folder[:-3]
    if os.listdir(folder) == []:
        try:
            os.rmdir(folder)
        except Exception as e:
            click.echo(f"    => {str(e)}")


def duration_in_seconds(time):
    return sum([pow(60,time.count(":")-i)*int(r) for i,r in enumerate(time.split(":"))])


def seconds_in_duration(seconds):
    return ":".join((
        f"{seconds//3600:02}",
        f"{(seconds-(seconds//3600*3600))//60:02}",
        f"{seconds-(seconds//3600*3600)-((seconds-(seconds//3600*3600))//60)*60:02}",
    ))
