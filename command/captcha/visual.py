# encoding: utf-8
"""
This module is used in order to create visual challenges.
"""

import os
import random
import time

import click
from flask import current_app
import requests
from bs4 import BeautifulSoup
from PIL import Image, ImageFilter

from app.model.visual_challenge import VisualChallengeModel
from app.model.visual_source import VisualSourceModel
from command.captcha import utils


WIKIMEDIA_URL = "https://commons.wikimedia.org/wiki/Special:Random/File"
SLEEP_TIME = 0.3
VISUAL_SOURCE_SIZE = 280
VISUAL_CHALLENGE_SIZE = 90


def get_number_of_active_challenges():
    return VisualChallengeModel.query.filter(
        VisualChallengeModel.used<current_app.config["MAX_USED"]
    ).count()


def get_inactive_challenges():
    return VisualChallengeModel.query.filter(
        VisualChallengeModel.used>=current_app.config["MAX_USED"]
    ).all()


def get_all_challenges():
    return VisualChallengeModel.query.all()


def get_picture():
    try:
        time.sleep(SLEEP_TIME)
        click.echo(".", nl=False)
        new_source = requests.get(WIKIMEDIA_URL, headers=utils.USER_AGENT)
        if new_source.url.lower()[-4:] in (".jpg", "jpeg", ".jpe"):
            # Source is a picture (or sort of) let's parse its metadata
            html = BeautifulSoup(new_source.text, "html.parser")
            picture = {}
            try:
                picture["file_url"] = html.find("a", class_="internal")["href"].strip()
                picture["author"] = html.find(
                    "td", id="fileinfotpl_aut"
                ).next_sibling.next_sibling.text.strip()
                licenses = html.find_all("span", class_="licensetpl_short")
            except Exception as e:
                click.echo(new_source.url)
                click.echo(f"    => {str(e)}")
                return None
            # License should be ok with re-use
            for picture_license in licenses:
                for license in utils.ACCEPTED_LICENSES:
                    if utils.normalize_license(
                        picture_license.text.strip().lower()
                    ).startswith(license):
                        picture["license"] = utils.normalize_license(
                            picture_license.text.strip().lower()
                        )
                        return picture
    except Exception as e:
        click.echo(new_source.url)
        click.echo(f"    => {str(e)}")


def create_source(picture):
    source = {}
    picture_id = "%064x" % random.getrandbits(256)
    picture_path = os.path.join(
        current_app.config["VISUAL_SOURCE_DIR"],
        picture_id[:2],
        picture_id[2:4],
        picture_id[4:6],
    )
    os.makedirs(picture_path)
    result = requests.get(picture["file_url"], stream=True, headers=utils.USER_AGENT)
    with open(f"{picture_path}/{picture_id}.jpg", "wb") as f:
        for chunk in result.iter_content(chunk_size=1048576):
            f.write(chunk)
    source["original"] = Image.open(f"{picture_path}/{picture_id}.jpg")
    if (
        (source["original"].size[0] <= VISUAL_SOURCE_SIZE) or
        (source["original"].size[1] <= VISUAL_SOURCE_SIZE)
    ):
        os.remove(f"{picture_path}/{picture_id}.jpg")
        utils.clean_folders(picture_path)
        return
    else:
        try:
            source["original"].thumbnail(
                (VISUAL_SOURCE_SIZE, VISUAL_SOURCE_SIZE), Image.ANTIALIAS
            )
            source["original"].save(f"{picture_path}/{picture_id}.jpg", "JPEG")
            source["source"] = VisualSourceModel(
                filename=f"{picture_id}.jpg",
                author=picture["author"].split("\n")[0],
                license=picture["license"],
            )
            source["source"].save()
        except Exception as e:
            os.remove(f"{picture_path}/{picture_id}.jpg")
            utils.clean_folders(picture_path)
            return
    return source


def get_sources():
    return VisualSourceModel.query.all()


def get_source_image(image_path):
    return Image.open(image_path)


def create_challenge(source_id, original):
    try:
        # Each challenge is a 120x120 picture blured at random
        token = "%064x" % random.getrandbits(256)
        pos_x = random.randint(0, original.size[0] - VISUAL_CHALLENGE_SIZE)
        pos_y = random.randint(0, original.size[1] - VISUAL_CHALLENGE_SIZE)
        challenge_path = os.path.join(
            current_app.config["VISUAL_CHALLENGE_DIR"],
            token[:2],
            token[2:4],
            token[4:6],
        )
        os.makedirs(challenge_path)
        # Do the crop, the blur, and save it on disk
        original.crop(
            (pos_x, pos_y, pos_x + VISUAL_CHALLENGE_SIZE, pos_y + VISUAL_CHALLENGE_SIZE)
        ).filter(ImageFilter.BLUR).save(f"{challenge_path}/{token}.jpg")
        # Then save it in database
        VisualChallengeModel(
            visual_source_id=source_id,
            used=0,
            token=token,
        ).save()
        return True
    except:
        return False


def delete_challenge(challenge):
    challenge_filepath = challenge.filepath
    source = challenge.visual_source
    challenge.delete()
    try:
        os.remove(challenge_filepath)
    except Exception as e:
        click.echo(f"    => {str(e)}")
    utils.clean_folders(os.path.dirname(challenge_filepath))
    if VisualChallengeModel.query.filter_by(visual_source_id=source.id).count() == 0:
        delete_source(source)


def delete_all_challenges():
    VisualChallengeModel.truncate()


def delete_source(source):
    source_filepath = source.filepath
    source.delete()
    try:
        os.remove(source_filepath)
    except Exception as e:
        click.echo(f"    => {str(e)}")
    utils.clean_folders(os.path.dirname(source_filepath))


def reset_challenges():
    VisualChallengeModel.execute(
        f"update {VisualChallengeModel.__tablename__} set used=0;"
    )
