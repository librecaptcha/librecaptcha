"""empty message

Revision ID: eeeeeeeeeeee
Revises: c6a080a95a87
Create Date: 2022-05-10 08:59:29.277953

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "eeeeeeeeeeee"
down_revision = "c6a080a95a87"
branch_labels = None
depends_on = None


def upgrade():
    # ### self writed commands ###
    op.execute("DELETE FROM settings;")
    op.execute("INSERT INTO settings VALUES (1, true, 0);")
    # ### end commands ###


def downgrade():
    # ### self writed commands ###
    op.execute("DELETE FROM settings;")
    # ### end commands ###
