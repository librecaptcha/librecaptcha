# encoding: utf-8

from datetime import datetime, timedelta
import secrets

from envelopes import Envelope
from envelopes.conn import SMTP
from flask import (
    current_app,
    flash,
    g,
    redirect,
    render_template,
    request,
    session,
    url_for,
)
from flask_babel import gettext as _
from flask_login import current_user, login_required, login_user, logout_user

from app.controller.controller import Controller
from app.form.key import KeyNewForm
from app.form.settings import SettingsForm
from app.form.user import (
    UserForgotPasswordForm,
    UserLoginForm,
    UserNewForm,
    UserResetPasswordForm,
    UserSubscribeForm,
)
from app.model.audio_challenge import AudioChallengeModel
from app.model.settings import SettingsModel
from app.model.user import UserModel
from app.model.visual_challenge import VisualChallengeModel


class User(Controller):
    def subscribe(self):
        g.settings = SettingsModel.query.first()
        g.subscription_available = (
            g.settings.enable_subscriptions
            and g.settings.max_accounts > len(UserModel.query.all())
        )
        g.form = UserSubscribeForm()
        if request.method == "POST":
            if g.form.validate_on_submit():
                user = UserModel.query.filter_by(login=g.form.login.data).first()
                if user is None:
                    if g.form.password.data == g.form.password_confirmation.data:
                        user = UserModel(
                            login=g.form.login.data,
                            password=g.form.password.data,
                            email="",
                            active=True,
                        )
                        user.save()
                        login_user(user)
                        return redirect(url_for("user.dashboard"))
                    else:
                        flash(_("Confirmation password is not identical to password."))
                else:
                    flash(_("This account exists already, pleace create another."))
            for (field, message) in g.form.errors.items():
                flash("%s: %s" % (field, message[0]))
        return render_template("user/subscribe.html")

    def forgot_password(self):
        g.form = UserForgotPasswordForm()
        if request.method == "POST":
            if g.form.validate_on_submit():
                user = UserModel.query.filter_by(login=g.form.login.data).first()
                if user is None:
                    user = UserModel.query.filter_by(email=g.form.login.data).first()
                if user is None:
                    # Security
                    # Do not tell spammers if an account exists or not
                    flash(_("An email has been sent to this account."))
                else:
                    if user.email == "":
                        flash(_("This account has no e-mail, please contact an admin."))
                    else:
                        # Sending email
                        smtp = SMTP(
                            current_app.config["SMTP_SERVER"],
                            login=current_app.config["SMTP_USER"],
                            password=current_app.config["SMTP_PASSWORD"],
                            tls=True,
                        )
                        subject = "Reset password"
                        user.forgot_token = secrets.token_hex(nbytes=20)
                        user.token_date = datetime.today()
                        user.save()
                        host_url = f"{request.scheme}://{request.headers['host']}"
                        link = f"{host_url}/reset?token={user.forgot_token}"
                        body_text = "Here is a link to reset your password: "
                        body_html = render_template(
                            "user/forgot_email.html",
                            context={
                                "name": user.login,
                                "reset_url": link,
                            },
                        )
                        smtp.send(
                            Envelope(
                                from_addr=(
                                    current_app.config["FROM_ADDRESS"],
                                    current_app.config["FROM_NAME"],
                                ),
                                to_addr=user.email,
                                subject=subject,
                                text_body=body_text,
                                html_body=body_html,
                            )
                        )
                        flash(_("An email has been sent to this account."))
                        return redirect(url_for("core.home"))
        return render_template("user/forgot_password.html")

    def reset_password(self):
        g.form = UserResetPasswordForm()
        if request.method == "POST":
            if g.form.validate_on_submit():
                # Token verification
                token = g.form.token.data
                if token is None:
                    flash(_("No token provided."))
                    return redirect(url_for("core.home"))
                user = UserModel.query.filter_by(forgot_token=token).first()
                if user is None:
                    flash(_("Token is invalid."))
                    return redirect(url_for("core.home"))
                if user.token_date < (datetime.today() - timedelta(days=1)) is None:
                    flash(_("Token is outdated."))
                    user.clear_token()
                    return redirect(url_for("core.home"))
                if g.form.password.data != g.form.password_confirmation.data:
                    flash(_("Passwords should be identical."))
                    return render_template("user/reset_password.html")
                # Reset token and save new password
                user.password = g.form.password.data
                user.clear_token()
                flash(_("New password set!"))
                return redirect(url_for("core.home"))
        g.form.token.data = request.args.get("token")
        return render_template("user/reset_password.html")

    def logout(self):
        session.clear()
        return redirect(url_for("core.home"))

    @login_required
    def dashboard(self):
        g.form_newkey = KeyNewForm()
        return render_template("user/dashboard.html")

    @login_required
    def admin(self):
        if not current_user.admin:
            return redirect(url_for("user.dashboard"))
        g.users = sorted(UserModel.query.all(), key=lambda x: x.login)
        g.settings = SettingsModel.query.first()
        g.audio_challenges = AudioChallengeModel.query.count()
        g.visual_challenges = VisualChallengeModel.query.count()
        g.form_newuser = UserNewForm()
        g.form_settings = SettingsForm(obj=g.settings)
        if request.method == "POST":
            if g.form_settings.validate_on_submit():
                if g.form_settings.max_accounts.data in (None, ""):
                    g.form_settings.max_accounts.data = 0
                if int(g.form_settings.max_accounts.data) < 0:
                    g.form_settings.max_accounts.data = 0
                g.settings.enable_subscriptions = g.form_settings.enable_subscriptions.data
                g.settings.max_accounts = g.form_settings.max_accounts.data
                g.settings.save()
        return render_template("user/admin.html")

    @login_required
    def add(self):
        if not current_user.admin:
            redirect(url_for("user.dashboard"))
        g.form = UserNewForm()
        if request.method == "POST":
            if g.form.validate_on_submit():
                user = UserModel.query.filter_by(login=g.form.login.data).first()
                if user is None:
                    UserModel(
                        login=g.form.login.data,
                        email=g.form.email.data,
                        active=g.form.active.data,
                        admin=g.form.admin.data,
                    ).save()
                    flash(_("New user added."))
                    return redirect(url_for("user.admin"))
                else:
                    flash(_("Account already exists, pleace choose another login."))
            for (field, message) in g.form.errors.items():
                flash("%s: %s" % (field, message[0]))
        return redirect(url_for("user.admin") + "#users")

    @login_required
    def enable(self, user_id):
        if not current_user.admin:
            redirect(url_for("user.dashboard"))
        if current_user.id != user_id:
            user = UserModel.query.get(user_id)
            user.active = True
            user.save()
        return redirect(url_for("user.admin") + "#users")

    @login_required
    def disable(self, user_id):
        if not current_user.admin:
            redirect(url_for("user.dashboard"))
        if current_user.id != user_id:
            user = UserModel.query.get(user_id)
            user.active = False
            user.save()
        return redirect(url_for("user.admin") + "#users")

    @login_required
    def delete(self, user_id):
        if not current_user.admin:
            redirect(url_for("user.dashboard"))
        if current_user.id != user_id:
            user = UserModel.query.get(user_id)
            user.delete()
        return redirect(url_for("user.admin") + "#users")

    @login_required
    def grant(self, user_id):
        if not current_user.admin:
            redirect(url_for("user.dashboard"))
        if current_user.id != user_id:
            user = UserModel.query.get(user_id)
            user.admin = True
            user.save()
        return redirect(url_for("user.admin") + "#users")

    @login_required
    def revoke(self, user_id):
        if not current_user.admin:
            redirect(url_for("user.dashboard"))
        if current_user.id != user_id:
            user = UserModel.query.get(user_id)
            user.admin = False
            user.save()
        return redirect(url_for("user.admin") + "#users")
