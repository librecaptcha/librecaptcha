# encoding: utf-8

from flask import flash, g, redirect, render_template, request, url_for
from flask_babel import gettext as _
from flask_login import login_user

from app.controller.controller import Controller
from app.form.user import UserLoginForm
from app.model.settings import SettingsModel
from app.model.user import UserModel


class Core(Controller):
    def home(self):
        settings = SettingsModel.query.first()
        g.subscription_available = (
            settings.enable_subscriptions
            and settings.max_accounts > len(UserModel.query.all())
        )
        g.form = UserLoginForm()
        if request.method == "POST":
            if g.form.validate_on_submit():
                user = UserModel.query.filter_by(login=g.form.login.data).first()
                if user and user.check_password(g.form.password.data):
                    if user.is_active:
                        login_user(user)
                        # Delete forgot_token if present
                        if user.forgot_token:
                            user.clear_token()
                        if user.admin:
                            return redirect(url_for("user.admin"))
                        return redirect(url_for("user.dashboard"))
                    else:
                        flash(_("Account disabled."))
                else:
                    flash(_("Bad user or password."))
            for (field, message) in g.form.errors.items():
                flash("%s: %s" % (field, message[0]))
        return render_template("core/home.html")
