# encoding: utf-8

import random
import re

from flask import current_app, g, redirect, render_template, request, url_for
from sqlalchemy.sql.expression import func

from app.controller.controller import Controller
from app.model.audio_challenge import AudioChallengeModel
from app.model.key import KeyModel
from app.model.visual_challenge import VisualChallengeModel


class Captcha(Controller):
    def librecaptcha(self):
        g.host_url = current_app.config["LIBRECAPTCHA_HOST"]
        return render_template("captcha/librecaptcha.js")

    def generate_challenge(self):
        site_host = re.search("^https?://([^:/]*).*$", request.referrer).group(1)
        site_key = KeyModel.query.filter_by(public=request.args.get("k")).first()
        if request.args.get("t") == "rc":
            g.classname = "g-recaptcha"
        else:
            g.classname = "librecaptcha"
        if site_key is None:
            # TODO: Return cleaner error message
            message = "Public Key not found"
            message += "\nKey       : %s" % request.args.get("k")
            message += "\nSite host : %s" % site_host
            print(message)
            g.error = "Public key '%s' is unknown." % request.args.get("k")
            return render_template("captcha/librecaptcha_error.js")
        elif site_host not in [domain.name for domain in site_key.domains.all()]:
            # TODO: Return cleaner error message
            message = "Domain not found"
            message += "\nKey       : %s" % request.args.get("k")
            message += "\nSite host : %s" % site_host
            message += "\nDomains   : %s" % [
                domain.name for domain in site_key.domains.all()
            ]
            print(message)
            g.error = "Domain '%s' is unknown for this public key." % site_host
            return render_template("captcha/librecaptcha_error.js")
        # Get a random visual challenge and disable it
        visual_challenge = (
            VisualChallengeModel.query.filter(
                VisualChallengeModel.used < current_app.config["MAX_USED"]
            )
            .order_by(func.random())
            .limit(1)
            .first()
        )
        visual_challenge.used = visual_challenge.used + 1
        visual_challenge.save()
        g.visual_source = visual_challenge.visual_source
        g.visual_challenges = [visual_challenge]
        for i in range(8):
            g.visual_challenges.append(
                    VisualChallengeModel.query.filter(
                    VisualChallengeModel.used < current_app.config["MAX_USED"]
                )
                .filter(
                    VisualChallengeModel.visual_source_id
                    != visual_challenge.visual_source_id
                )
                .order_by(func.random())
                .limit(1)
                .first()
            )
        random.shuffle(g.visual_challenges)
        # Get a random audio challenge and disable it
        g.audio_challenge = (
            AudioChallengeModel.query.filter(
                AudioChallengeModel.used < current_app.config["MAX_USED"]
            )
            .order_by(func.random())
            .limit(1)
            .first()
        )
        g.audio_challenge.used = g.audio_challenge.used + 1
        g.audio_challenge.save()

        g.host_url = current_app.config["LIBRECAPTCHA_HOST"]
        # Adapt answer with captcha type if necessary
        if request.args.get("t") == "rc":
            return render_template("captcha/get_challenge_recaptcha.js")
        return render_template("captcha/get_challenge.js")


