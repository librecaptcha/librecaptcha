# encoding: utf-8


class Controller:
    @classmethod
    def as_view(cls, method_name, *class_args, **class_kwargs):
        # Create the view function to return
        def view(*args, **kwargs):
            self = view.view_class(*class_args, **class_kwargs)
            if hasattr(self, method_name):
                return getattr(self, method_name)(*args, **kwargs)
            else:
                return "Class %s has no method called %s" % (
                    cls.__name__,
                    method_name,
                )

        view.view_class = cls
        # name used for endpoint : class name + method name
        view.__name__ = ".".join((cls.__name__.lower(), method_name))
        view.__doc__ = cls.__doc__
        view.__module__ = cls.__module__
        return view
