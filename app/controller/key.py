# encoding: utf-8

from datetime import datetime, timedelta
import secrets

from flask import flash, g, redirect, request, url_for
from flask_babel import gettext as _
from flask_login import current_user, login_required

from app.controller.controller import Controller
from app.form.key import KeyNewForm
from app.model.domain import DomainModel
from app.model.key import KeyModel


class Key(Controller):
    @login_required
    def add(self):
        g.form_newkey = KeyNewForm()
        if request.method == "POST":
            if g.form_newkey.validate_on_submit():
                # Verifying existing domains
                for name in g.form_newkey.name.data.split(","):
                    domain = DomainModel.query.join(
                        KeyModel, DomainModel.key_id==KeyModel.id
                    ).filter(
                        KeyModel.user_id==current_user.id
                    ).filter(DomainModel.name==name.strip()).first()
                    if domain is not None:
                        flash(f"Domain {domain} exists already, please delete its key before re-adding it.")
                        return redirect(url_for("user.dashboard") + "#keys")
                # Creating new key for those domains
                key = KeyModel(
                    public=secrets.token_hex(nbytes=20),
                    secret=secrets.token_hex(nbytes=20),
                    user_id=current_user.id,
                )
                key.save()
                # Saving domains
                for name in g.form_newkey.name.data.split(","):
                    domain = DomainModel(
                        name=name.strip(),
                        key_id=key.id,
                    )
                    domain.save()
            for (field, message) in g.form_newkey.errors.items():
                flash("%s: %s" % (field, message[0]))
        return redirect(url_for("user.dashboard") + "#keys")

    @login_required
    def delete(self, key_id):
        key = KeyModel.query.get(key_id)
        if current_user.id == key.user_id:
            domains = DomainModel.query.filter_by(key_id=key.id).all()
            for domain in domains:
                domain.delete()
            key.delete()
        return redirect(url_for("user.dashboard") + "#keys")
