document.getElementsByClassName("librecaptcha")[0].innerHTML="
<div style='width:290px;border:3px solid #bbf;border-radius:8px;background:#eef;font-size:10pt;font-family:Helvetica'>
    <div id='home'>
        <span style='display:block;width:280px;margin:.3em auto;text-align:center;cursor:pointer' onclick='document.getElementById(\"home\").style.display=\"none\";document.getElementById(\"challenge\").style.display=\"block\";'><strong>Click here to resolve Challenge</strong></span>
    </div>

    <div id='challenge' style='display:none'>
        <table style='width:100%' cellspacing='0' cellpadding='0'>
        <tr>
            <td style='text-align:center;background:#88c;color:#fff;padding:.3em;cursor:pointer' onclick='document.getElementById(\"visual_challenge\").style.display=\"block\";document.getElementById(\"audio_challenge\").style.display=\"none\"'>Visual challenge</td>
            <td style='text-align:center;background:#88c;color:#fff;padding:.3em;cursor:pointer' onclick='document.getElementById(\"visual_challenge\").style.display=\"none\";document.getElementById(\"audio_challenge\").style.display=\"block\"'>Audio challenge</td>
        </tr>
        </table>
        <div>
            <div id='visual_challenge'>
            <table style='width: 280px;margin:.3em auto'>
            <tr>
                <td colspan='3' style='padding:0 1em'><strong>Select thumbnail corresponding to the main picture:</strong></td>
            </tr>
            <tr>
                <td colspan='3'><img style='height:280px;width:280px' src='{{g.host_url}}{{url_for('static', filename=g.visual_source.fileurl)}}' title='{{g.visual_source.author}} - {{g.visual_source.license}}'/></td>
            </tr>
            {%for visual_challenge in g.visual_challenges%}
            {%if loop.index%3==1%}
            <tr>
            {%endif%}
                <td><label for='answer_{{loop.index}}' style='background:url({{g.host_url}}{{url_for('static', filename=visual_challenge.fileurl)}});display:block;width:90px;height:90px;cursor:pointer'><input type='radio' name='librecaptcha-answer' id='answer_{{loop.index}}' value='{{visual_challenge.token}}'/></label></td>
            {%if loop.index%3==0%}
            </tr>
            {%endif%}
            {%endfor%}
            </table>
            </div>

            <div id='audio_challenge' style='display:none'>
            <table style='width: 280px;margin:.3em auto'>
            <tr>
                <td style='padding:0 1em'><strong>Listen next sound and type in short and long beeps respectively with dots (.) and dashes (-):</strong></td>
            </tr>
            <tr>
                <td>
                <audio controls style='width:280px' onplay='document.getElementById(\"librecaptcha-answer-audio\").focus()'><source src='{{g.host_url}}{{url_for('static', filename=g.audio_challenge.fileurl)}}' type='audio/ogg'>Sorry, your browser does not support the audio element.</audio>
                <input type='text' id='librecaptcha-answer-audio' name='librecaptcha-answer' value='' placeholder='{{g.audio_challenge.author}} - {{g.audio_challenge.license}}' style='width:262px;margin-top:.6em' />
                </td>
            </tr>
            </table>
            </div>
        </div>
    </div>
</div>
";
