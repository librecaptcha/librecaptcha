function load_captcha(){
// Get sitekey parameter
var lc = document.getElementsByClassName("librecaptcha")[0];
// Add own js
var nl = document.createElement("script");
nl.type = "text/javascript";
nl.async = true;
nl.defer = true;
nl.src = "{{g.host_url}}/generatechallenge.js?t=lc&k="+lc.getAttribute("data-sitekey");
var s0 = document.getElementsByTagName("script")[0];
s0.parentNode.insertBefore(nl, s0);
};
if(window.addEventListener){window.addEventListener("load",load_captcha)}else{window.attachEvent("onload",load_captcha)};
