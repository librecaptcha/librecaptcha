# encoding: utf-8

from flask_babel import gettext as _
from flask_wtf import FlaskForm
from wtforms import BooleanField, HiddenField, PasswordField, StringField
from wtforms.validators import DataRequired


class UserLoginForm(FlaskForm):
    login = StringField(_("Login"), validators=[DataRequired()])
    password = PasswordField(_("Password"), validators=[DataRequired()])


class UserSubscribeForm(FlaskForm):
    login = StringField(_("Login"), validators=[DataRequired()])
    password = PasswordField(_("Password"), validators=[DataRequired()])
    password_confirmation = PasswordField(
        _("Password confirmation"),
        validators=[DataRequired()],
    )
    email = PasswordField(
        _("Email"),
        description=_("Optional, used only for password recovery"),
    )


class UserForgotPasswordForm(FlaskForm):
    login = StringField(_("Login or Email"), validators=[DataRequired()])


class UserResetPasswordForm(FlaskForm):
    token = HiddenField()
    password = PasswordField(_("New password"), validators=[DataRequired()])
    password_confirmation = PasswordField(
        _("Password confirmation"),
        validators=[DataRequired()],
    )


class UserNewForm(FlaskForm):
    login = StringField(_("Login"), validators=[DataRequired()])
    email = StringField(_("Email address"))
    active = BooleanField(_("Active"))
    admin = BooleanField(_("Admin"))
