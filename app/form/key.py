# encoding: utf-8

from flask_babel import gettext as _
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms.validators import DataRequired


class KeyNewForm(FlaskForm):
    name = StringField(_("Name"), validators=[DataRequired()])
