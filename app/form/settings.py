# encoding: utf-8

from flask_babel import gettext as _
from flask_wtf import FlaskForm
from wtforms import BooleanField, StringField


class SettingsForm(FlaskForm):
    enable_subscriptions = BooleanField(_("Enable subscriptions"))
    max_accounts = StringField(_("Max accounts authorized"))
