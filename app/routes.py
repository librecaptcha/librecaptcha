# encoding: utf-8

from app.controller.captcha import Captcha
from app.controller.core import Core
from app.controller.key import Key
from app.controller.user import User


# Listing endpoints
routes = [
    ("/", Core.as_view("home"), ["GET", "POST"]),
    ("/subscribe", User.as_view("subscribe"), ["GET", "POST"]),
    ("/forgot", User.as_view("forgot_password"), ["GET", "POST"]),
    ("/reset", User.as_view("reset_password"), ["GET", "POST"]),
    ("/logout", User.as_view("logout")),
    ("/admin", User.as_view("admin"), ["GET", "POST"]),
    ("/dashboard", User.as_view("dashboard"), ["GET", "POST"]),
    ("/add_user", User.as_view("add"), ["POST"]),
    ("/enable_user/<int:user_id>", User.as_view("enable")),
    ("/disable_user/<int:user_id>", User.as_view("disable")),
    ("/delete_user/<int:user_id>", User.as_view("delete")),
    ("/grant_user/<int:user_id>", User.as_view("grant")),
    ("/revoke_user/<int:user_id>", User.as_view("revoke")),
    ("/add_key", Key.as_view("add"), ["POST"]),
    ("/delete_key/<int:key_id>", Key.as_view("delete")),
    # Pages for captcha
    ("/librecaptcha.js", Captcha.as_view("librecaptcha")),
    ("/generatechallenge.js", Captcha.as_view("generate_challenge")),
    ("/verifychallenge", Captcha.as_view("verify_challenge"), ["POST"]),
]

# Listing API endpoints
apis = []
