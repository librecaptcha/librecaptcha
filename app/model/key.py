# encoding: utf-8

from app import db
from app.model.model import Model


class KeyModel(db.Model, Model):
    __tablename__ = "key"
    id = db.Column(db.Integer, primary_key=True)
    public = db.Column(db.String(40))
    secret = db.Column(db.String(40))
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    user = db.relationship("UserModel", backref=db.backref("keys", lazy="dynamic"))

    @property
    def displayed(self):
        return sum([domain.displayed for domain in self.domains.all()])

    @property
    def failed(self):
        return sum([domain.failed for domain in self.domains.all()])

    @property
    def successful(self):
        return self.displayed - self.failed
