# encoding: utf-8

from app import db
from app.model.model import Model


class SettingsModel(db.Model, Model):
    __tablename__ = "settings"
    id = db.Column(db.Integer, primary_key=True)
    enable_subscriptions = db.Column(db.Boolean, default=True)
    max_accounts = db.Column(db.Integer, default=100)
