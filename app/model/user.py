# encoding: utf-8

import bcrypt

from flask import current_app

from app import db
from app.model.model import Model


def get_user(user_id):
    return UserModel.query.get(user_id)


class UserModel(db.Model, Model):
    __tablename__ = "user"
    id = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.String(500), unique=True)
    password_hash = db.Column(db.String(128), default="")
    email = db.Column(db.String(500), default="")
    active = db.Column(db.Boolean, default=False)
    admin = db.Column(db.Boolean, default=False)
    forgot_token = db.Column(db.String(40))
    token_date = db.Column(db.DateTime)

    @property
    def password(self):
        return self.password_hash

    @password.setter
    def password(self, password):
        self.password_hash = bcrypt.hashpw(
            password.encode("utf-8"),
            bcrypt.gensalt(rounds=int(current_app.config["BCRYPT_ROUNDS"])),
        ).decode("UTF-8")

    def check_password(self, password):
        return bcrypt.checkpw(
            password.encode("utf-8"),
            self.password_hash.encode("utf-8"),
        )

    @property
    def is_active(self):
        return self.active or False

    @property
    def is_anonymous(self):
        return self.id is None

    @property
    def is_authenticated(self):
        return self.id is not None

    @property
    def domains(self):
        return [domain for key in self.keys.all() for domain in key.domains.all()]

    def get_id(self):
        return str(self.id)

    def clear_token(self):
        self.forgot_token = None
        self.token_date = None
        self.save()
