# encoding: utf-8

from app import db
from app.model.model import Model


class SolutionModel(db.Model, Model):
    __tablename__ = "solution"
    id = db.Column(db.Integer, primary_key=True)
    public_key = db.Column(db.String(40))
    domain_name = db.Column(db.String(1000))
    token_solution = db.Column(db.String(64))
    visual_solution = db.Column(db.String(64))
    audio_solution = db.Column(db.String(100))
    creation_date = db.Column(db.DateTime)
