# encoding: utf-8

import os

from flask import current_app

from app import db
from app.model.model import Model


class VisualSourceModel(db.Model, Model):
    __tablename__ = "visual_source"
    id = db.Column(db.Integer, primary_key=True)
    filename = db.Column(db.String(1000))
    author = db.Column(db.String(1000))
    license = db.Column(db.String(1000))

    @property
    def filepath(self):
        return os.path.join(
            current_app.config["VISUAL_SOURCE_DIR"],
            self.filename[:2],
            self.filename[2:4],
            self.filename[4:6],
            self.filename,
        )

    @property
    def fileurl(self):
        return os.path.join(
            current_app.config["VISUAL_SOURCE_URL"],
            self.filename[:2],
            self.filename[2:4],
            self.filename[4:6],
            self.filename,
        )
