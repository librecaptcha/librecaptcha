# encoding: utf-8
"""
This module imports models to allow alembic and flask to find them.
"""

from app.model.audio_challenge import AudioChallengeModel
from app.model.domain import DomainModel
from app.model.key import KeyModel
from app.model.settings import SettingsModel
from app.model.solution import SolutionModel
from app.model.user import UserModel
from app.model.visual_source import VisualSourceModel
from app.model.visual_challenge import VisualChallengeModel
