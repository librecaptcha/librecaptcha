# encoding: utf-8

from datetime import datetime

from app import db


class Model:
    def save(self):
        is_existing = True
        if len(self.__table__.primary_key.columns.keys()) > 1:
            is_existing = False
        else:
            for column_name in self.__table__.primary_key.columns.keys():
                column = self.__getattribute__(column_name)
                if column is None or column == "":
                    is_existing = False
        if not is_existing:
            db.session.add(self)
        db.session.commit()

    def delete(self):
        is_existing = True
        for column_name in self.__table__.primary_key.columns.keys():
            column = self.__getattribute__(column_name)
            if column is None or column == "":
                is_existing = False
        if is_existing:
            db.session.delete(self)
            db.session.commit()

    @classmethod
    def execute(self, query):
        db.session.execute(query)
        db.session.commit()

    @classmethod
    def truncate(self):
        db.session.execute(f"truncate table {self.__tablename__};")
        db.session.commit()

    def serialize(self):
        result_dict = {}
        for key in self.__table__.columns.keys():
            if not key.startswith("_"):
                if isinstance(getattr(self, key), datetime):
                    result_dict[key] = getattr(self, key).strftime("%Y-%m-%d")
                else:
                    result_dict[key] = getattr(self, key)
        return result_dict
