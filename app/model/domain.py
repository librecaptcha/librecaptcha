# encoding: utf-8

from app import db
from app.model.model import Model


class DomainModel(db.Model, Model):
    __tablename__ = "domain"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(1000))
    key_id = db.Column(db.Integer, db.ForeignKey("key.id"))
    key = db.relationship("KeyModel", backref=db.backref("domains", lazy="dynamic"))
    displayed = db.Column(db.Integer, default=0)
    failed = db.Column(db.Integer, default=0)
    resolved_visual = db.Column(db.Integer, default=0)
    resolved_audio = db.Column(db.Integer, default=0)

    def __repr__(self):
        return self.name
