# encoding: utf-8

import os

from flask import current_app

from app import db
from app.model.model import Model


class VisualChallengeModel(db.Model, Model):
    __tablename__ = "visual_challenge"
    id = db.Column(db.Integer, primary_key=True)
    token = db.Column(db.String(64))
    used = db.Column(db.Integer, default=0)
    visual_source_id = db.Column(db.Integer, db.ForeignKey("visual_source.id"))
    visual_source = db.relationship(
        "VisualSourceModel", backref=db.backref("challenges", lazy="dynamic")
    )

    @property
    def filename(self):
        return "%s.jpg" % self.token

    @property
    def filepath(self):
        return os.path.join(
            current_app.config["VISUAL_CHALLENGE_DIR"],
            self.filename[:2],
            self.filename[2:4],
            self.filename[4:6],
            self.filename,
        )

    @property
    def fileurl(self):
        return os.path.join(
            current_app.config["VISUAL_CHALLENGE_URL"],
            self.filename[:2],
            self.filename[2:4],
            self.filename[4:6],
            self.filename,
        )
