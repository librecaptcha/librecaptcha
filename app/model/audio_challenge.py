# encoding: utf-8

import os

from flask import current_app

from app import db
from app.model.model import Model


class AudioChallengeModel(db.Model, Model):
    __tablename__ = "audio_challenge"
    id = db.Column(db.Integer, primary_key=True)
    token = db.Column(db.String(64))
    used = db.Column(db.Integer, default=0)
    date = db.Column(db.DateTime)
    solution = db.Column(db.String(20))
    author = db.Column(db.String(1000))
    license = db.Column(db.String(1000))

    @property
    def filename(self):
        return "%s.ogg" % self.token

    @property
    def filepath(self):
        return os.path.join(
            current_app.config["AUDIO_CHALLENGE_DIR"],
            self.filename[:2],
            self.filename[2:4],
            self.filename[4:6],
            self.filename,
        )

    @property
    def fileurl(self):
        return os.path.join(
            current_app.config["AUDIO_CHALLENGE_URL"],
            self.filename[:2],
            self.filename[2:4],
            self.filename[4:6],
            self.filename,
        )
