# encoding: utf-8

import os


BASE_DIR = os.path.abspath(os.path.dirname(__file__))
DEBUG = os.environ.get("DEBUG", False)
SECRET_KEY = os.environ.get("DEBUG", "Choose a secret key")
JINJA_ENV = {
    "TRIM_BLOCKS": True,
    "LSTRIP_BLOCKS": True,
}
SQLALCHEMY_DATABASE_URI = os.environ.get(
    "SQLALCHEMY_DATABASE_URI",
    "sqlite:///" + os.path.join(BASE_DIR, "db.sqlite3"),
)
SQLALCHEMY_TRACK_MODIFICATIONS = False
BCRYPT_ROUNDS = os.environ.get("BCRYPT_ROUNDS", 15)
BABEL_DEFAULT_LOCALE = os.environ.get("BABEL_DEFAULT_LOCALE", "fr")
AVAILABLE_LANGUAGES = {
    "fr": "Français",
    # ~ "en": "English",
}

# E-mail settings
FROM_ADDRESS = os.environ.get("FROM_ADDRESS", "contact@librecaptcha.org")
FROM_NAME = os.environ.get("FROM_NAME", "LibreCaptcha")
SMTP_SERVER = os.environ.get("SMTP_SERVER", "localhost")
SMTP_USER = os.environ.get("SMTP_USER", "")
SMTP_PASSWORD = os.environ.get("SMTP_PASSWORD", "")

# Challenges settings
MAX_USED = int(os.environ.get("MAX_USED", "10"))
VISUAL_CHALLENGE_BY_SOURCE = int(os.environ.get("VISUAL_CHALLENGE_BY_SOURCE", "40"))
VISUAL_CHALLENGE_MIN = int(os.environ.get("VISUAL_CHALLENGE_MIN", "1000"))
VISUAL_CHALLENGE_MAX = int(os.environ.get("VISUAL_CHALLENGE_MAX", "2000"))
VISUAL_CHALLENGE_URL = os.environ.get("VISUAL_CHALLENGE_URL", "data/visual/challenges")
VISUAL_CHALLENGE_DIR = os.path.abspath(f"{BASE_DIR}/static/{VISUAL_CHALLENGE_URL}")
VISUAL_SOURCE_URL = os.environ.get("VISUAL_SOURCE_URL", "data/visual/sources")
VISUAL_SOURCE_DIR = os.path.abspath(f"{BASE_DIR}/static/{VISUAL_SOURCE_URL}")
AUDIO_CHALLENGE_BY_SOURCE = int(os.environ.get("AUDIO_CHALLENGE_BY_SOURCE", "100"))
AUDIO_CHALLENGE_MIN = int(os.environ.get("AUDIO_CHALLENGE_MIN", "1000"))
AUDIO_CHALLENGE_MAX = int(os.environ.get("AUDIO_CHALLENGE_MAX", "2000"))
AUDIO_CHALLENGE_URL = os.environ.get("AUDIO_CHALLENGE_URL", "data/audio/challenges")
AUDIO_CHALLENGE_DIR = os.path.abspath(f"{BASE_DIR}/static/{AUDIO_CHALLENGE_URL}")
AUDIO_SOURCE_URL = os.environ.get("AUDIO_SOURCE_URL", "data/audio/sources")
AUDIO_SOURCE_DIR = os.path.abspath(f"{BASE_DIR}/static/{AUDIO_SOURCE_URL}")

# Duration of solution before returning a False result (in minutes)
SOLUTION_DURATION = int(os.environ.get("SOLUTION_DURATION", "2"))

# LibreCaptcha settings
LIBRECAPTCHA_HOST = "http://localhost:5000"
